#!/usr/bin/env iocsh.bash

require(modbus)
require(s7plc)
require(calc)

epicsEnvSet(utg-ymir:chop-chic-01_VERSION,"plcfactory")

iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

iocshLoad("./utg-ymir_chop-chic-01.iocsh","IPADDR=172.30.239.17,RECVTIMEOUT=3000")

